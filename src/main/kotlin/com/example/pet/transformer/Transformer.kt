package com.example.pet.transformer

interface Transformer <A, B> {
    fun transform (source: A): B
}