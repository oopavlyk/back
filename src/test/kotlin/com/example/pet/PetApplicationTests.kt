package com.example.pet

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(
	webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
class PetApplicationTests {

	@Test
	fun contextLoads() {
	}

}
