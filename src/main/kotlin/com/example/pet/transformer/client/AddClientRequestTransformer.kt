package com.example.pet.transformer.client

import com.example.pet.dto.client.AddClientRequest
import com.example.pet.entities.Client
import com.example.pet.transformer.Transformer
import org.springframework.stereotype.Component

@Component
class AddClientRequestTransformer: Transformer<AddClientRequest, Client> {
    override fun transform(source: AddClientRequest): Client = Client(
        firstName = source.firstName,
        lastName = source.lastName,
        email = source.email,
        password = source.password,
        birthday = source.birthday
    )
}