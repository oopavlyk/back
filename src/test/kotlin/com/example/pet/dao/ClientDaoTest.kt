package com.example.pet.dao

import com.example.pet.entities.Client
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.data.repository.findByIdOrNull
import java.util.*
import kotlin.test.assertEquals

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ClientDaoTest(
    @Autowired private val clientDao: ClientDao
) {
//
    @Test
    fun createAndGet(){
        val client = Client("email", "password", Date(), "firstName", "lastName")
        clientDao.save(client)
        val fetchedClient = clientDao.findByIdOrNull(client.id)

        assertEquals(client, fetchedClient!!)
    }

    @Test
    fun updateAndGet(){
        val client = Client("email", "password", Date(), "firstName", "lastName")
        clientDao.save(client)
        var fetchedClient = clientDao.findByIdOrNull(client.id)

        assertEquals(client, fetchedClient!!)

        client.firstName = "bob"
        clientDao.save(client)
        fetchedClient = clientDao.findByIdOrNull(client.id)

        assertEquals(client, fetchedClient!!)
    }

    @Test
    fun deleteAndGet(){
        val client = Client("email", "password", Date(), "firstName", "lastName")
        clientDao.save(client)
        var fetchedClient = clientDao.findByIdOrNull(client.id)

        assertEquals(client, fetchedClient!!)

        clientDao.delete(client)

        fetchedClient = clientDao.findByIdOrNull(fetchedClient.id)

        assertEquals(null, fetchedClient)
    }
}