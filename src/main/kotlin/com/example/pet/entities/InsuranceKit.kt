package com.example.pet.entities

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "insurance_kit")
class InsuranceKit(

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long? = null,

    var duration: Int,

    var compensationPercent: Int,

    var damageLevel: String,

    var coveredPart: String,
)
