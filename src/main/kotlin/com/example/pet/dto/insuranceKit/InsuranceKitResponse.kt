package com.example.pet.dto.insuranceKit

data class InsuranceKitResponse(
    val id: Long,
    val duration: Int,
    val compensationPercent: Int,
    val damageLevel: String,
    val coveredPart: String
)
