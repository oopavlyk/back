package com.example.pet.dto.vehicle

import com.example.pet.dto.client.ClientVehicleResponse
import com.example.pet.entities.InsuranceContract
import java.util.*

data class VehicleResponse(
    val id: Long,
    val color: String,
    val engineSize: Double,
    val yearOfManufacturer: Date,
    val weight: Int,
    val client: ClientVehicleResponse?,
    val insuranceContract: InsuranceContract?
)
