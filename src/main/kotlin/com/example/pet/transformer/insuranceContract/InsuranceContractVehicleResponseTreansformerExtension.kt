package com.example.pet.transformer.insuranceContract

import com.example.pet.dto.client.ClientVehicleResponse
import com.example.pet.dto.insuranceContract.InsuranceContractVehicleResponse
import com.example.pet.dto.insuranceKit.InsuranceKitResponse
import com.example.pet.entities.InsuranceContract

fun InsuranceContract.toInsuranceContractVehicleResponse(): InsuranceContractVehicleResponse {
    return InsuranceContractVehicleResponse(
        clientVehicleResponse = this.client!!.let {
            ClientVehicleResponse(
                id = it.id,
                firstName = it.firstName,
                lastName = it.lastName,
                birthday = it.birthday,
            )
        },
        insuranceKit = this.insuranceKit.map { insuranceKit ->
            insuranceKit.let {
                InsuranceKitResponse(
                    insuranceKit.id!!,
                    insuranceKit.duration,
                    insuranceKit.compensationPercent,
                    insuranceKit.damageLevel,
                    it.coveredPart)
            }
        }.toMutableList()
    )
}