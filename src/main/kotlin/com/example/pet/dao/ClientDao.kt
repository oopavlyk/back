package com.example.pet.dao

import com.example.pet.entities.Client
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface ClientDao : PagingAndSortingRepository<Client, Long>{
    fun findByHiddenFalse(): List<Client>
    fun findByHiddenIsFalse(pageable: Pageable): Page<Client>

    @Query(value = "SELECT c.first_name FROM client AS c WHERE c.first_name like '%' || ?1 || '%' LIMIT 5", nativeQuery = true)
    fun findByName(name: String): List<String>
}
