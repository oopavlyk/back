package com.example.pet.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class VehicleAlreadyHasContract(id: Long) : RuntimeException("Vehicle (id = $id) already has contract") {
}