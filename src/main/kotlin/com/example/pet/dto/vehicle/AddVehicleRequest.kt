package com.example.pet.dto.vehicle

import java.util.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Size

data class AddVehicleRequest(
    @field:NotEmpty
    @field:Size(min = 2, max = 32)
    val color: String,

    val engineSize: Double,

    @field:NotNull
    @field:Past
    val yearOfManufacturer: Date,

    val weight: Int
)