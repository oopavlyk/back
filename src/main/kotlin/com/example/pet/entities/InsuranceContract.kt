package com.example.pet.entities

import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.*
import org.hibernate.envers.Audited
import org.hibernate.envers.NotAudited

@Entity
@Audited
@Table(name = "insurance_contract")
class InsuranceContract(

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    val client: Client?,

    @JsonManagedReference
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vehicle_id")
    var vehicle: Vehicle?,

    @ManyToMany
    @NotAudited
    var insuranceKit: MutableList<InsuranceKit> = mutableListOf()
) : BaseEntity() {
    @Id
    @SequenceGenerator(name = "insuranceContractSeqGen", sequenceName = "insuranceContractSeq", initialValue = 300, allocationSize = 1)
    @GeneratedValue(generator = "insuranceContractSeqGen")
    override var id: Long = 0
}
