package com.example.pet.controller

import com.example.pet.dto.insuranceKit.InsuranceKitResponse
import com.example.pet.service.InsuranceKitService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/insuranceKit")
class InsuranceKitController(
    private val insuranceKitService: InsuranceKitService
) {

    @GetMapping
    fun list(): MutableList<InsuranceKitResponse> {
        return insuranceKitService.list()
    }
}