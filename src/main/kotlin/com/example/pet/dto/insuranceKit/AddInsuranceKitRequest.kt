package com.example.pet.dto.insuranceKit

data class AddInsuranceKitRequest(
    val id: Long,
    val duration: Int,
    val compensationPercent: Int,
    val damageLevel: String,
    val coveredPart: String
)
