package com.example.pet.dto.client

import java.util.*

data class ClientVehicleResponse(
    val id: Long,
    val firstName: String,
    val lastName: String,
    val birthday: Date
)
