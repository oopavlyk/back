package com.example.pet.controller

import com.example.pet.dto.insuranceContract.AddInsuranceContractRequest
import com.example.pet.dto.insuranceContract.InsuranceContractClientResponse
import com.example.pet.dto.insuranceContract.InsuranceContractVehicleResponse
import com.example.pet.service.InsuranceContractService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/insuranceContract")
class InsuranceContractController(
    private val insuranceContractService: InsuranceContractService
) {

    @GetMapping("/client/{id}")
    fun getClient(
        @PathVariable id: Long
    ): MutableList<InsuranceContractClientResponse> {
        return insuranceContractService.findByClient(id)
    }

    @GetMapping("/vehicle/{id}")
    fun getVehicle(
        @PathVariable id: Long
    ): InsuranceContractVehicleResponse? {
        return insuranceContractService.findByVehicle(id)
    }

    @PostMapping
    fun create(
        @RequestBody addInsuranceContractRequest: AddInsuranceContractRequest
    ) {
        insuranceContractService.create(addInsuranceContractRequest)
    }
}