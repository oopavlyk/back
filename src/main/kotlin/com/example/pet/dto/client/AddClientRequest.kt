package com.example.pet.dto.client

import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class AddClientRequest(
    @field:NotEmpty(message = "client.firstname is empty")
    @field:Size(min = 2, max = 32, message = "client.firstname less then 2 or greater then 32")
    val firstName: String,

    @field:NotEmpty(message = "client.lastname is empty")
    @field:Size(min = 2, max = 32, message = "client.lastname less then 2 or greater then 32")
    val lastName: String,

    @field:NotBlank
    @field:NotEmpty(message = "client.email is empty")
    val email: String,

    @field:NotEmpty(message = "client.password empty")
    @field:Size(min = 2, max = 32, message = "client.password less then 2 or greater then 32")
    var password: String,

    @field:NotNull(message = "client.birthday is null")
    val birthday: Date
)