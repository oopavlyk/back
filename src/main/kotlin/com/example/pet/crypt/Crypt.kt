package com.example.pet.crypt


object Crypt {
    fun encrypt(password: String, key: Int) : String{
        val encoded = with(StringBuffer()) {
            for (c in password.toCharArray()) {
                var result = c.code
                result += key
                this.append(result.toChar())
            }
            this.toString()
        }
        return encoded;
    }

    fun decrypt(password: String, key: Int): String {
        val decoded = with(StringBuffer()) {
            for (c in password.toCharArray()) {
                var result = c.code
                result -= key
                this.append(result.toChar())
            }
            this.toString()
        }
        return decoded
    }
}