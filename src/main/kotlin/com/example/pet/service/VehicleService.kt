package com.example.pet.service

import com.example.pet.dto.vehicle.AddVehicleRequest
import com.example.pet.dto.vehicle.UpdateVehicleRequest
import com.example.pet.dto.vehicle.VehicleClientResponse
import com.example.pet.dto.vehicle.VehicleResponse
import org.springframework.data.domain.Page

interface VehicleService {
    fun findAll(): List<VehicleResponse>

    fun get(number: Int, size: Int, sort: String): Page<VehicleClientResponse>

    fun findAllNullClient(): List<VehicleResponse>

    fun findWithoutContract(clientId: Long): List<VehicleResponse>

    fun findById(id: Long): VehicleResponse

    fun create(request: AddVehicleRequest)

    fun update(request: UpdateVehicleRequest)

    fun delete(id: Long)
}