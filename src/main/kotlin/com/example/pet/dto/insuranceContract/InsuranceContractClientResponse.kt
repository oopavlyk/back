package com.example.pet.dto.insuranceContract

import com.example.pet.dto.insuranceKit.InsuranceKitResponse
import com.example.pet.dto.vehicle.VehicleClientResponse

data class InsuranceContractClientResponse(
    val vehicleClientResponse: VehicleClientResponse,
    val insuranceKit: MutableList<InsuranceKitResponse> = mutableListOf()
)
