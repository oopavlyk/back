package com.example.pet.controller

import com.example.pet.dto.client.AddClientRequest
import com.example.pet.dto.client.ClientResponse
import com.example.pet.dto.client.UpdateClientRequest
import com.example.pet.service.impl.ClientServiceImpl
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.util.*

@AutoConfigureMockMvc
@SpringBootTest
internal class ClientControllerTest(
    @Autowired
    val mockMvc: MockMvc
) {
    @MockBean
    lateinit var clientServiceImpl: ClientServiceImpl

    @Test
    fun get_byId(){
        val client = ClientResponse(24, "","",Date(), listOf())
        `when`(clientServiceImpl.findById(24)).thenReturn(client)
        mockMvc.get("/client/24"){
            contentType = MediaType.APPLICATION_JSON
            accept = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            content { contentType(MediaType.APPLICATION_JSON) }
        }
        verify(clientServiceImpl, times(1)).findById(24)
    }

    @Test
    fun get_all(){
        val response: List<ClientResponse> = listOf()
        `when`(clientServiceImpl.findAll()).thenReturn(response)
        mockMvc.get("/client"){
            contentType = MediaType.APPLICATION_JSON
            accept = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            /*content { string(containsString("id")) }
            content { string(containsString("firstName")) }
            content { string(containsString("lastName")) }
            content { string(containsString("birthday")) }
            content { string(containsString("vehicles")) }*/
            content { contentType(MediaType.APPLICATION_JSON) }
        }
        verify(clientServiceImpl, times(1)).findAll()
    }

    @Test
    fun post_nokTest(){
        post_nokTest(
            "",
            "last",
            "email@email",
            "password",
            Date(),
            MockMvcResultMatchers.status().isBadRequest
        )
        post_nokTest(
            "first",
            "",
            "email@email",
            "password",
            Date(),
            MockMvcResultMatchers.status().isBadRequest
        )
        post_nokTest(
            "first",
            "last",
            "",
            "password",
            Date(),
            MockMvcResultMatchers.status().isBadRequest
        )
        post_nokTest(
            "first",
            "last",
            "email@email",
            "",
            Date(),
            MockMvcResultMatchers.status().isBadRequest
        )
    }

    private fun post_nokTest(
        firstName: String,
        lastName: String,
        email: String,
        password: String,
        birthday: Date,
        status: ResultMatcher){
        val client = AddClientRequest(
            firstName,
            lastName,
            email,
            password,
            birthday
        )
        val jsonData = jacksonObjectMapper().writeValueAsString(client)
        mockMvc.post("/client") {
            contentType = MediaType.APPLICATION_JSON
            content = jsonData
            accept = MediaType.APPLICATION_JSON
        }.andDo { print() }.andExpect {
            status { match(status) }
        }
    }

    @Test
    fun post_okTest(){
        post_okTest(
            "first",
            "last",
            "email@email",
            "password",
            Date(),
            MockMvcResultMatchers.status().isCreated
        )
    }

    private fun post_okTest(
        firstName: String,
        lastName: String,
        email: String,
        password: String,
        birthday: Date,
        status: ResultMatcher){
        val client = AddClientRequest(
            firstName,
            lastName,
            email,
            password,
            birthday
        )
        doNothing().`when`(clientServiceImpl).create(client)
        val jsonData = jacksonObjectMapper().writeValueAsString(client)
        mockMvc.post("/client") {
            contentType = MediaType.APPLICATION_JSON
            content = jsonData
            accept = MediaType.APPLICATION_JSON
        }.andDo { print() }.andExpect {
            status { match(status) }
        }
        verify(clientServiceImpl, times(1)).create(client)
    }

    @Test
    fun put_test(){
        put(
            24,
            "",
            "last",
            Date(),
            MockMvcResultMatchers.status().isOk
        )
        put(
            24,
            "first",
            "",
            Date(),
            MockMvcResultMatchers.status().isOk
        )
        put(
            24,
            "first",
            "last",
            Date(),
            MockMvcResultMatchers.status().isOk
        )
    }

    private fun put(
        id: Long,
        firstName: String,
        lastName: String,
        birthday: Date,
        status: ResultMatcher
    ){
        val client = UpdateClientRequest(
            id,
            firstName,
            lastName,
            birthday,
            null
        )
        doNothing().`when`(clientServiceImpl).update(client)
        val jsonData = jacksonObjectMapper().writeValueAsString(client)
        mockMvc.put("/client"){
            contentType = MediaType.APPLICATION_JSON
            content = jsonData
            accept = MediaType.APPLICATION_JSON
        }.andDo { print() }.andExpect {
            status { match(status) }
        }
        verify(clientServiceImpl, times(1)).update(client)
    }

    @Test
    fun delete_test(){
        doNothing().`when`(clientServiceImpl).delete(24)
        mockMvc.delete("/client/{id}", 24){
            accept = MediaType.APPLICATION_JSON
        }.andDo { print() }.andExpect {
            status { isOk() }
        }
        verify(clientServiceImpl, times(1)).delete(24)
    }

    @Test
    fun getPaged(){
        val page: Page<ClientResponse> = PageImpl(listOf())
        `when`(clientServiceImpl.getClient(0,5,"sort.asc")).thenReturn(page)
        mockMvc.get("/client/paged?"){
            accept = MediaType.APPLICATION_JSON
        }.andDo { print() }.andExpect {
            status { isOk() }
        }
        verify(clientServiceImpl, times(1)).getClient(0,5,"id.desc")
    }

}