package com.example.pet.dao

import com.example.pet.dto.insuranceKit.InsuranceKitResponse
import com.example.pet.entities.InsuranceKit
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface InsuranceKitDao: CrudRepository<InsuranceKit, Long> {
    fun findAllByOrderByDuration(): List<InsuranceKitResponse>
    fun findByIdIn(ids: List<Long>): List<InsuranceKit>
}