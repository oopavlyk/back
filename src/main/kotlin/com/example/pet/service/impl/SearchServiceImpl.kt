package com.example.pet.service.impl

import com.example.pet.dao.ClientDao
import com.example.pet.dao.VehicleDao
import com.example.pet.service.SearchService
import org.springframework.stereotype.Service

@Service
class SearchServiceImpl (
    private val clientDao: ClientDao,
    private val vehicleDao: VehicleDao
    ): SearchService {

    override fun findByNameForClient(name: String): List<String> = clientDao.findByName(name)
}