package com.example.pet.transformer.vehicle

import com.example.pet.dto.client.ClientVehicleResponse
import com.example.pet.dto.vehicle.VehicleResponse
import com.example.pet.entities.Vehicle

fun Vehicle.toVehicleResponse(): VehicleResponse{
    return VehicleResponse(
        id = this.id ,
        color = this.color,
        engineSize = this.engineSize,
        weight = this.weight,
        yearOfManufacturer = this.yearOfManufacture,
        insuranceContract = this.insuranceContract,
        client = this.client?.let {
            ClientVehicleResponse(
                it.id,
                it.firstName,
                it.lastName,
                it.birthday)
        }
    )
}