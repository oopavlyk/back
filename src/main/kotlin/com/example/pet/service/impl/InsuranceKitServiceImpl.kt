package com.example.pet.service.impl

import com.example.pet.dao.InsuranceKitDao
import com.example.pet.dto.insuranceKit.InsuranceKitResponse
import com.example.pet.entities.InsuranceKit
import com.example.pet.service.InsuranceKitService
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class InsuranceKitServiceImpl(private val insuranceKitDao: InsuranceKitDao) : InsuranceKitService {


    override fun findById(id: Long): InsuranceKit? {
        return insuranceKitDao.findByIdOrNull(id)
    }

    override fun create(request: InsuranceKit) {
        val insuranceKit = InsuranceKit(
            duration = request.duration,
            compensationPercent = request.compensationPercent,
            damageLevel = request.damageLevel,
            coveredPart = request.coveredPart)

        saveOrUpdate(insuranceKit)
    }

    override fun list(): MutableList<InsuranceKitResponse> {
        val insuranceKits: MutableList<InsuranceKitResponse> = mutableListOf()
        for (kit in insuranceKitDao.findAllByOrderByDuration())
            insuranceKits.add(
                InsuranceKitResponse(
                    id = kit.id,
                    duration = kit.duration,
                    compensationPercent = kit.compensationPercent,
                    damageLevel = kit.damageLevel,
                    coveredPart = kit.coveredPart
                ))
        return insuranceKits
    }

    private fun saveOrUpdate(insuranceKit: InsuranceKit) {
        insuranceKitDao.save(insuranceKit)
    }

    fun findByIdIn(ids: List<Long>): List<InsuranceKit> {
        return insuranceKitDao.findByIdIn(ids)
    }
}