package com.example.pet.dto.insuranceContract

import com.example.pet.dto.insuranceKit.AddInsuranceKitRequest

data class AddInsuranceContractRequest(
    val clientId: Long,
    val vehicleId: Long,
    val insuranceKit: MutableList<AddInsuranceKitRequest> = mutableListOf()
)
