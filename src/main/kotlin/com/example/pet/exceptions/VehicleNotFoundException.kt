package com.example.pet.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class VehicleNotFoundException(id: Long) : RuntimeException("Vehicle (id = $id) not found") {
}