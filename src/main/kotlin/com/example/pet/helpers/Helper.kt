package com.example.pet.helpers

import com.example.pet.validator.Validation
import org.springframework.data.domain.Sort

object Helper {

    fun getSort(sort: String): Sort {
        Validation.validForSort(sort)
        val propertie = sort.split('.')[0]
        val direction = sort.split('.')[1]
        return Sort.by(
            getDirection(direction),
            propertie
        )
    }

    fun getDirection(direction: String): Sort.Direction {
        return if (direction == "desc")
            Sort.Direction.DESC
        else
            Sort.Direction.ASC
    }
}