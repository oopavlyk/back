package com.example.pet.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class InsuranceKitNotFoundException(id: Long) : RuntimeException("InsuranceKit (id = $id) not found") {
}