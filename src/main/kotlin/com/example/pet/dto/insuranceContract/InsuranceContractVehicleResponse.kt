package com.example.pet.dto.insuranceContract

import com.example.pet.dto.client.ClientVehicleResponse
import com.example.pet.dto.insuranceKit.InsuranceKitResponse

data class InsuranceContractVehicleResponse(
    val clientVehicleResponse: ClientVehicleResponse,
    val insuranceKit: MutableList<InsuranceKitResponse>? = mutableListOf()
)
