package com.example.pet.service.impl

import com.example.pet.crypt.Crypt
import com.example.pet.dao.ClientDao
import com.example.pet.dto.client.AddClientRequest
import com.example.pet.dto.client.ClientResponse
import com.example.pet.dto.client.UpdateClientRequest
import com.example.pet.entities.Client
import com.example.pet.exceptions.ClientNotFoundException
import com.example.pet.helpers.Helper
import com.example.pet.service.ClientService
import com.example.pet.transformer.client.AddClientRequestTransformer
import com.example.pet.transformer.client.toClientResponse
import com.example.pet.validator.Validation
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class ClientServiceImpl(
    private val clientDao: ClientDao,
    private val addClientRequestTransformer: AddClientRequestTransformer,
    private val vehicleServiceImpl: VehicleServiceImpl
) : ClientService {

    /*fun get(number: Int, size: Int, sort: String): Page<ClientVehicleResponse>{
        ClientValidation().validForSort(sort
        return clientDao.get(PageRequest.of(number, size, getSort(sort)), sort)
    }*/

    override fun create(request: AddClientRequest) {
        request.password = Crypt.encrypt(request.password, request.firstName.hashCode())
        saveOrUpdate(addClientRequestTransformer.transform(request))
    }

    override fun getClient(number: Int, size: Int, sort: String): Page<ClientResponse> {
        return clientDao.findByHiddenIsFalse(PageRequest.of(
            number,
            size,
            Helper.getSort(sort)
        )).map(Client::toClientResponse)
    }

    override fun delete(id: Long) {
        val client = clientDao.findByIdOrNull(id) ?: throw ClientNotFoundException(id)

        client.let {
            it.vehicles.forEach { v ->
                v.client = null
                v.insuranceContract = null
            }
            it.insuranceContracts.forEach { c ->
                c.vehicle = null
                c.hidden = true
            }
            it.hidden = true
        }

        clientDao.save(client)
    }

    override fun findAll(): List<ClientResponse> {
        return clientDao.findByHiddenFalse().map(Client::toClientResponse)
    }

    override fun findById(id: Long): ClientResponse {
        val client = clientDao.findByIdOrNull(id)
        Validation.validClientForReturn(client)
        return client!!.toClientResponse()
    }

    override fun update(request: UpdateClientRequest) {
        val client = clientDao.findByIdOrNull(request.id)
        Validation.validClientForReturn(client)

        if (request.vehicleId != null)
            vehicleServiceImpl.atachClient(request.vehicleId!!, client!!)

        client!!.apply {
            firstName = request.firstName
            lastName = request.lastName
            birthday = request.birthday
        }

        saveOrUpdate(client)
    }

    private fun saveOrUpdate(client: Client) {
        clientDao.save(client)
    }

    fun findByIdForContract(id: Long): Client {
        return clientDao.findByIdOrNull(id) ?: throw ClientNotFoundException(id)
    }

}
