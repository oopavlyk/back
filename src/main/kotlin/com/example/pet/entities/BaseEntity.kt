package com.example.pet.entities

import java.time.LocalDateTime
import java.util.*
import javax.persistence.MappedSuperclass
import javax.persistence.PrePersist
import javax.persistence.PreUpdate
import org.hibernate.envers.Audited

@MappedSuperclass
@Audited
abstract class BaseEntity {

    abstract var id: Long

    val uuid: UUID = UUID.randomUUID()

    var createdAt: LocalDateTime = LocalDateTime.now()

    var updatedAt: LocalDateTime = LocalDateTime.now()

    var hidden: Boolean = false

    @PrePersist
    fun onCreate() {
        this.createdAt = LocalDateTime.now()
        this.updatedAt = LocalDateTime.now()
    }

    @PreUpdate
    fun onUpdate() {
        this.updatedAt = LocalDateTime.now()
    }
}
