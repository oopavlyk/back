package com.example.pet.transformer.client

import com.example.pet.dto.client.ClientResponse
import com.example.pet.dto.vehicle.VehicleClientResponse
import com.example.pet.entities.Client

fun Client.toClientResponse(): ClientResponse{
    return ClientResponse(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        birthday = this.birthday,
        vehicles = this.vehicles.map {
            VehicleClientResponse(
                it.id,
                it.color,
                it.engineSize,
                it.yearOfManufacture,
                it.weight
            )
        }
    )
}