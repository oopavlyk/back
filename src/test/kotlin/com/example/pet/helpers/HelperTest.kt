package com.example.pet.helpers

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.Sort

@AutoConfigureMockMvc
@SpringBootTest
internal class HelperTest {

    private val helper = Helper

    @Test
    fun getSort_test() {
        var sort = "sort.asc"
        assertEquals(helper.getSort(sort), Sort.by(Sort.Direction.ASC, "sort"))
        sort = "sort.desc"
        assertEquals(helper.getSort(sort), Sort.by(Sort.Direction.DESC, "sort"))
    }

    @Test
    fun getDirection() {
        var direction = "asc"
        assertEquals(helper.getDirection(direction), Sort.Direction.ASC)
        direction = "desc"
        assertEquals(helper.getDirection(direction), Sort.Direction.DESC)
    }
}