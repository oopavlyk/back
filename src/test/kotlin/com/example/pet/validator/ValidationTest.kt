package com.example.pet.validator

import com.example.pet.entities.Client
import com.example.pet.entities.InsuranceContract
import com.example.pet.entities.Vehicle
import com.example.pet.exceptions.ClientNotFoundException
import com.example.pet.exceptions.VehicleAlreadyHasContract
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.test.assertFailsWith

internal class ValidationTest {

    private val validation = Validation

    @Test
    fun validForSort_exceptionCheck() {
        assertFailsWith(
            exceptionClass = IllegalArgumentException::class,
            message = "Invalid sort parameter passed, not contains '.'",
            block = {validation.validForSort("sort")}
        )
        assertFailsWith(
            exceptionClass = IllegalArgumentException::class,
            message = "Invalid sort parameter passed, not contains 'asc' or 'desc'",
            block = {validation.validForSort("sort.")}
        )
    }

    @Test
    fun validClientForReturn() {
        val client = Client("email", "password", Date(), "first", "last")
        client.hidden = true
        assertFailsWith(
            exceptionClass = ClientNotFoundException::class,
            message = "Client (id = ${client.id}) not found",
            block = {validation.validClientForReturn(client)}
        )
        assertFailsWith(
            exceptionClass = IllegalArgumentException::class,
            message = "Can`t return Client. Client is null",
            block = {validation.validClientForReturn(null)}
        )
    }

    @Test
    fun validVehicleForContract() {
        val insuranceContract = InsuranceContract(null, null)
        val vehicle = Vehicle("color", 1.1, Date(), 12, insuranceContract)
        assertFailsWith(
            exceptionClass = VehicleAlreadyHasContract::class,
            message = "Vehicle (id = ${vehicle.id}) already has contract",
            block = {validation.validVehicleForContract(vehicle)}
        )
        vehicle.insuranceContract = null
        assertEquals(validation.validVehicleForContract(vehicle), vehicle)
    }

    @Test
    fun validVehicleForAtachment() {
        val client = Client("email", "password", Date(), "first", "last")
        val vehicle = Vehicle("color", 1.1, Date(), 12, null, client)
        assertFailsWith(
            exceptionClass = VehicleAlreadyHasContract::class,
            message = "Vehicle (id = ${vehicle.id}) already has contract",
            block = {validation.validVehicleForAtachment(vehicle)}
        )
        vehicle.client = null
        assertEquals(validation.validVehicleForAtachment(vehicle), vehicle)
    }
}