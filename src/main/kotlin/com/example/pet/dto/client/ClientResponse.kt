package com.example.pet.dto.client

import com.example.pet.dto.vehicle.VehicleClientResponse
import java.util.*

data class ClientResponse(
    val id: Long,
    val firstName: String,
    val lastName: String,
    val birthday: Date,

    val vehicles: List<VehicleClientResponse>
)