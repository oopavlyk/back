package com.example.pet.dao

import com.example.pet.entities.Vehicle
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface VehicleDao: CrudRepository<Vehicle, Long>, VehicleRepositoryList {
    fun findAllByOrderById(): List<Vehicle>
    fun findByClientIdAndInsuranceContractIsNullAndHiddenIsFalseOrClientIdIsNullAndHiddenIsFalse(client_id: Long): List<Vehicle>
    fun findByClientIsNullAndHiddenIsFalse(): List<Vehicle>
    fun findAllByHiddenIsFalse(): List<Vehicle>
    fun findByClientId(client_id: Long): List<Vehicle>
}