package com.example.pet.service

import com.example.pet.dto.insuranceContract.AddInsuranceContractRequest
import com.example.pet.dto.insuranceContract.InsuranceContractClientResponse
import com.example.pet.dto.insuranceContract.InsuranceContractVehicleResponse

interface InsuranceContractService {

    fun findByVehicle(id: Long): InsuranceContractVehicleResponse?

    fun findByClient(id: Long): MutableList<InsuranceContractClientResponse>

    fun create(request: AddInsuranceContractRequest)
}