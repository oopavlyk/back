package com.example.pet.dto.client

import java.util.*

data class UpdateClientRequest(
    val id: Long,
    val firstName: String,
    val lastName: String,
    var birthday: Date,
    var vehicleId: Long?

)