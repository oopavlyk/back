package com.example.pet.dto.vehicle

data class UpdateVehicleRequest(
    val id: Long,
    val color: String,
    val engineSize: Double,
    val weight: Int,
)