package com.example.pet.dao

import com.example.pet.entities.InsuranceContract
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface InsuranceContractDao: CrudRepository<InsuranceContract, Long> {
    fun findAllByClientIdAndHiddenIsFalse(id: Long): MutableList<InsuranceContract?>
    fun findByVehicleIdAndHiddenIsFalse(id: Long): InsuranceContract?
}