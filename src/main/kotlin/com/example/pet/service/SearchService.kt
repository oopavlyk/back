package com.example.pet.service

interface SearchService {

    fun findByNameForClient(name: String): List<String>
}