package com.example.pet.service.impl

import com.example.pet.dao.InsuranceContractDao
import com.example.pet.dto.insuranceContract.AddInsuranceContractRequest
import com.example.pet.dto.insuranceContract.InsuranceContractClientResponse
import com.example.pet.dto.insuranceContract.InsuranceContractVehicleResponse
import com.example.pet.entities.InsuranceContract
import com.example.pet.service.InsuranceContractService
import com.example.pet.transformer.insuranceContract.toInsuranceContractClientResponse
import com.example.pet.transformer.insuranceContract.toInsuranceContractVehicleResponse
import com.example.pet.validator.Validation
import org.springframework.stereotype.Service

@Service
class InsuranceContractServiceImpl(
    private val insuranceContractDao: InsuranceContractDao,
    private val clientServiceImpl: ClientServiceImpl,
    private val vehicleServiceImpl: VehicleServiceImpl,
    private val insuranceKitServiceImpl: InsuranceKitServiceImpl
) : InsuranceContractService {

    override fun create(request: AddInsuranceContractRequest) {
        val vehicle = vehicleServiceImpl.findByIdForContract(request.vehicleId)
        val client = clientServiceImpl.findByIdForContract(request.clientId)

        vehicleServiceImpl.atachClient(
            Validation.validVehicleForContract(vehicle),
            client
        )

        insuranceContractDao.save(InsuranceContract(
            client,
            vehicle,
            insuranceKitServiceImpl.findByIdIn(request.insuranceKit
                .map {
                    it.id
                }
            ).toMutableList()
        ))
    }

    override fun findByVehicle(id: Long): InsuranceContractVehicleResponse? {
        return insuranceContractDao.findByVehicleIdAndHiddenIsFalse(id)?.toInsuranceContractVehicleResponse()

    }

    override fun findByClient(id: Long): MutableList<InsuranceContractClientResponse> {
        val insuranceContracts = mutableListOf<InsuranceContractClientResponse>()

        for (contract in insuranceContractDao.findAllByClientIdAndHiddenIsFalse(id))
            if (contract != null)
                insuranceContracts.add(contract.toInsuranceContractClientResponse())

        return insuranceContracts.toMutableList()
    }
}