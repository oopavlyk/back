package com.example.pet.controller

import com.example.pet.dto.vehicle.AddVehicleRequest
import com.example.pet.dto.vehicle.UpdateVehicleRequest
import com.example.pet.dto.vehicle.VehicleClientResponse
import com.example.pet.service.VehicleService
import javax.validation.Valid
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/vehicle")
class VehicleController(
    private val vehicleService: VehicleService
) {

    @GetMapping("/criteria")
    fun get(@RequestParam(defaultValue = "0", name = "page") page: Int,
            @RequestParam(defaultValue = "5", name = "size") size: Int,
            @RequestParam(defaultValue = "id.desc", name = "sort") sort: String
    ): Page<VehicleClientResponse> {
        return vehicleService.get(page, size, sort)
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun list() = vehicleService.findAll()

    @GetMapping("/free")
    fun listFree() = vehicleService.findAllNullClient()

    @GetMapping("/forContract/client/{id}")
    fun listForContract(@PathVariable id: Long) = vehicleService.findWithoutContract(id)

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = vehicleService.findById(id)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@Valid @RequestBody addVehicleRequest: AddVehicleRequest) {
        vehicleService.create(addVehicleRequest)
    }

    @PutMapping
    fun update(@RequestBody updateVehicleRequest: UpdateVehicleRequest) {
        vehicleService.update(updateVehicleRequest)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = vehicleService.delete(id)
}