package com.example.pet.entities

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import java.util.*
import javax.persistence.*
import org.hibernate.envers.Audited

@SequenceGenerator(name = "")
@Entity
@Audited
@Table(name = "client")
class Client(

    var email: String,
    var password: String,
    var birthday: Date,
    var firstName: String,
    var lastName: String,

    @JsonBackReference
    @OneToMany(mappedBy = "client")
    var insuranceContracts: MutableList<InsuranceContract> = mutableListOf(),

    @JsonManagedReference
    @OneToMany(mappedBy = "client", cascade = [CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH], fetch = FetchType.EAGER)
    var vehicles: MutableList<Vehicle> = mutableListOf()
) : BaseEntity() {
    @Id
    @SequenceGenerator(name = "clientSeqGen", sequenceName = "clientSeq", initialValue = 100, allocationSize = 1)
    @GeneratedValue(generator = "clientSeqGen")
    override var id: Long = 0
}
