package com.example.pet.entities

import com.fasterxml.jackson.annotation.JsonBackReference
import java.util.*
import javax.persistence.*
import org.hibernate.envers.Audited

@Entity
@Audited
@Table(name = "vehicle")
class Vehicle(

    var color: String,
    var engineSize: Double,
    var yearOfManufacture: Date,
    var weight: Int,

    @JsonBackReference
    @OneToOne(mappedBy = "vehicle")
    var insuranceContract: InsuranceContract? = null,

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    var client: Client? = null
) : BaseEntity() {
    @Id
    @SequenceGenerator(name = "vehicleSeqGen", sequenceName = "vehicleSeq", initialValue = 200, allocationSize = 4)
    @GeneratedValue(generator = "vehicleSeqGen")
    override var id: Long = 0
}
