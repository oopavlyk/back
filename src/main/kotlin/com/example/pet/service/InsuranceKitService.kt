package com.example.pet.service

import com.example.pet.dto.insuranceKit.InsuranceKitResponse
import com.example.pet.entities.InsuranceKit

interface InsuranceKitService {

    fun findById(id: Long): InsuranceKit?

    fun create(request: InsuranceKit)

    fun list(): MutableList<InsuranceKitResponse>
}