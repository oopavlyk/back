package com.example.pet.impl

import com.example.pet.dao.ClientDao
import com.example.pet.dto.client.AddClientRequest
import com.example.pet.dto.client.ClientResponse
import com.example.pet.dto.client.UpdateClientRequest
import com.example.pet.entities.Client
import com.example.pet.entities.Vehicle
import com.example.pet.helpers.Helper
import com.example.pet.service.impl.ClientServiceImpl
import com.example.pet.service.impl.VehicleServiceImpl
import com.example.pet.transformer.client.AddClientRequestTransformer
import com.example.pet.transformer.client.toClientResponse
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import java.util.*

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class ClientServiceImplTest {

    @MockK
    lateinit var addClientRequestTransformer: AddClientRequestTransformer

    @MockK
    lateinit var vehicleServiceImpl: VehicleServiceImpl

    @MockK
    lateinit var clientDao: ClientDao

    @InjectMockKs
    lateinit var clientServiceImpl: ClientServiceImpl

    @Test
    fun create(){
        val addClient = AddClientRequest("first", "last", "email", "password", Date())

        val transformed = mockk<Client>()

        every { addClientRequestTransformer.transform(addClient) } returns transformed
        every { clientDao.save(transformed) } returns transformed

        clientServiceImpl.create(addClient)

        verify {clientDao.save(transformed)  }
        verify {addClientRequestTransformer.transform(addClient)  }
    }

    @Test
    fun getClient(){
        val client1 = Client("email", "password", Date(),"first1", "last1", mutableListOf(), mutableListOf<Vehicle>())
        val client2 = Client("email", "password", Date(),"first2", "last2", mutableListOf(), mutableListOf<Vehicle>())
        val client3 = Client("email", "password", Date(),"first3", "last3", mutableListOf(), mutableListOf<Vehicle>())

        val list = listOf(client1,client2,client3)
        val pageable = PageRequest.of(0,5, Helper.getSort("id.desc"))

        val page: Page<Client> = PageImpl(list)

        every { clientDao.findByHiddenIsFalse(pageable) } returns page

        clientServiceImpl.getClient(0,5,"id.desc")

        verify { clientDao.findByHiddenIsFalse(pageable) }
    }

    @Test
    fun delete(){
        val client = Client("email", "password", Date(),"first1", "last1", mutableListOf(), mutableListOf())

        val deletedClient = client
        client.hidden = true

        every { clientDao.findByIdOrNull(client.id) } returns client
        every { clientDao.save(client) } returns deletedClient

        clientServiceImpl.delete(client.id)

        assertEquals(client, deletedClient)

        verify { clientDao.findByIdOrNull(client.id) }
        verify { clientDao.save(client) }
    }

    @Test
    fun getAll(){
        val list: MutableList<Client> = mutableListOf()
        val client1 = Client("email", "password", Date(),"first1", "last1", mutableListOf(), mutableListOf<Vehicle>())
        val client2 = Client("email", "password", Date(),"first2", "last2", mutableListOf(), mutableListOf<Vehicle>())
        val client3 = Client("email", "password", Date(),"first3", "last3", mutableListOf(), mutableListOf<Vehicle>())

        list.add(client1)
        list.add(client2)
        list.add(client3)

        list.forEach(Client::toClientResponse)

        every { clientDao.findByHiddenFalse() } returns list

        val responseList: List<ClientResponse> = clientServiceImpl.findAll()

        assertEquals(3, responseList.size)

        verify { clientDao.findByHiddenFalse() }
    }

    @Test
    fun getOne(){
        val client1 = Client("email", "password", Date(),"first1", "last1", mutableListOf(), mutableListOf<Vehicle>())

        client1.id = 1
        client1.hidden = false

        every { clientDao.findByIdOrNull(client1.id) } returns client1

        clientServiceImpl.findById(client1.id)

        verify { clientDao.findByIdOrNull(client1.id) }
    }

    @Test
    fun update(){
        val client1 = Client("email", "password", Date(),"first1", "last1", mutableListOf(), mutableListOf<Vehicle>())
        client1.hidden = false
        client1.id = 0

        val updateClient = UpdateClientRequest(client1.id, "first", "last", Date(),null)

        every { clientDao.findByIdOrNull(updateClient.id) } returns client1
        every { clientDao.save(client1) } returns client1

        clientServiceImpl.update(updateClient)

        verify { clientDao.save(client1) }
    }

    @Test
    fun fundByIdForContract(){
        val client = mockk<Client>()
        every { clientDao.findByIdOrNull(any()) } returns client

        clientServiceImpl.findByIdForContract(1)
        verify { clientDao.findByIdOrNull(any()) }
    }
}