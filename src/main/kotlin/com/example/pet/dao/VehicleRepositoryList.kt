package com.example.pet.dao

import com.example.pet.dto.vehicle.VehicleClientResponse
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface VehicleRepositoryList {

    fun get(pageable: Pageable, sort: String): Page<VehicleClientResponse>
}