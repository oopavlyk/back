package com.example.pet.service.impl

import com.example.pet.dao.VehicleDao
import com.example.pet.dto.vehicle.AddVehicleRequest
import com.example.pet.dto.vehicle.UpdateVehicleRequest
import com.example.pet.dto.vehicle.VehicleClientResponse
import com.example.pet.dto.vehicle.VehicleResponse
import com.example.pet.entities.Client
import com.example.pet.entities.Vehicle
import com.example.pet.exceptions.VehicleNotFoundException
import com.example.pet.helpers.Helper
import com.example.pet.service.VehicleService
import com.example.pet.transformer.vehicle.AddVehicleRequestTransformer
import com.example.pet.transformer.vehicle.toVehicleResponse
import com.example.pet.validator.Validation
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class VehicleServiceImpl(
    private val vehicleDao: VehicleDao,
    private val addVehicleRequestTransformer: AddVehicleRequestTransformer
) : VehicleService {

    override fun get(number: Int, size: Int, sort: String): Page<VehicleClientResponse> {
        Validation.validForSort(sort)
        return vehicleDao.get(
            PageRequest.of(
                number,
                size,
                Helper.getSort(sort)),
            sort
        )
    }

    override fun findAll(): List<VehicleResponse> {
        return vehicleDao.findAllByHiddenIsFalse().map(Vehicle::toVehicleResponse)
    }

    override fun findAllNullClient(): List<VehicleResponse> {
        return vehicleDao.findByClientIsNullAndHiddenIsFalse().map(Vehicle::toVehicleResponse)
    }

    override fun findWithoutContract(clientId: Long): List<VehicleResponse> {
        return vehicleDao
            .findByClientIdAndInsuranceContractIsNullAndHiddenIsFalseOrClientIdIsNullAndHiddenIsFalse(clientId)
            .map {
                it.toVehicleResponse()
            }
    }

    override fun findById(id: Long): VehicleResponse {
        return vehicleDao.findByIdOrNull(id)?.toVehicleResponse() ?: throw VehicleNotFoundException(id)
    }

    override fun create(request: AddVehicleRequest) {
        saveOrUpdate(addVehicleRequestTransformer.transform(request))
    }

    override fun update(request: UpdateVehicleRequest) {
        val vehicle = vehicleDao.findByIdOrNull(request.id) ?: throw VehicleNotFoundException(request.id)

        saveOrUpdate(vehicle.apply {
            vehicle.color = request.color
            vehicle.engineSize = request.engineSize
            vehicle.weight = request.weight
        })
    }

    override fun delete(id: Long) {
        val vehicle = vehicleDao.findByIdOrNull(id) ?: throw VehicleNotFoundException(id)

        vehicle.let {
            it.client = null
            it.insuranceContract?.hidden = true
            it.insuranceContract = null
            it.hidden = true
        }

        saveOrUpdate(vehicle)
    }

    private fun saveOrUpdate(vehicle: Vehicle) {
        vehicleDao.save(vehicle)
    }

    fun findByIdForContract(id: Long): Vehicle {
        return vehicleDao.findByIdOrNull(id) ?: throw VehicleNotFoundException(id)
    }

    fun atachClient(vehicle: Vehicle, clientP: Client) {
        saveOrUpdate(vehicle.apply {
            client = clientP
        })
    }

    fun atachClient(vehicleId: Long, clientP: Client) {
        saveOrUpdate(
            Validation
                .validVehicleForAtachment(
                    findByIdForContract(vehicleId)
                ).apply {
                    client = clientP
                })
    }
}