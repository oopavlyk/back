package com.example.pet.transformer.vehicle

import com.example.pet.dto.vehicle.AddVehicleRequest
import com.example.pet.entities.Vehicle
import com.example.pet.transformer.Transformer
import org.springframework.stereotype.Component

@Component
class AddVehicleRequestTransformer: Transformer<AddVehicleRequest, Vehicle> {
    override fun transform(source: AddVehicleRequest): Vehicle {
        return Vehicle(
            color = source.color,
            engineSize = source.engineSize,
            weight = source.weight,
            yearOfManufacture = source.yearOfManufacturer
        )
    }
}