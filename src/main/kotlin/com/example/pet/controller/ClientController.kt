package com.example.pet.controller


import com.example.pet.dto.client.AddClientRequest
import com.example.pet.dto.client.ClientResponse
import com.example.pet.dto.client.UpdateClientRequest
import com.example.pet.service.ClientService
import javax.validation.Valid
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@RequestMapping("/client")
class ClientController(
    private val clientService: ClientService
) {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun list() = clientService.findAll()

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = clientService.findById(id)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@Valid @RequestBody addClientRequest: AddClientRequest) {
        return clientService.create(addClientRequest)
    }

    @PutMapping
    fun update(@RequestBody updateClientRequest: UpdateClientRequest) {
        clientService.update(updateClientRequest)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = clientService.delete(id)


    @GetMapping("/paged")
    fun getPage(
        @RequestParam(defaultValue = "0", name = "page") page: Int,
        @RequestParam(defaultValue = "5", name = "size") size: Int,
        @RequestParam(defaultValue = "id.desc", name = "sort") sort: String
    ): Page<ClientResponse> {
        return clientService.getClient(page, size, sort)
    }
}