package com.example.pet.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class SortInvalidParamsException(sort: String) : RuntimeException("Invalid value for sort. Value can`t be: ($sort)")