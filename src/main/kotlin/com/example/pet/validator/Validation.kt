package com.example.pet.validator

import com.example.pet.entities.Client
import com.example.pet.entities.Vehicle
import com.example.pet.exceptions.ClientNotFoundException
import com.example.pet.exceptions.VehicleAlreadyHasContract

object Validation {

    fun validForSort(sort: String){
        if (!sort.contains('.'))
            throw IllegalArgumentException("Invalid sort parameter passed, not contains '.'")
        else {
            val direction = sort.split('.')[1]
            if (!(direction.contains("asc") || direction.contains("desc")))
                throw IllegalArgumentException("Invalid sort parameter passed, not contains 'asc' or 'desc'")
        }
    }

    fun validClientForReturn(client: Client?){
        if (client != null) {
            if (client.hidden)
                throw ClientNotFoundException(client.id)
        }
        else
            throw IllegalArgumentException("Can`t return Client. Client is null")
    }

    fun validVehicleForContract(vehicle: Vehicle): Vehicle {
        if (vehicle.insuranceContract != null)
            throw VehicleAlreadyHasContract(vehicle.id)
        return vehicle
    }

    fun validVehicleForAtachment(vehicle: Vehicle): Vehicle {
        if (vehicle.client != null)
            throw VehicleAlreadyHasContract(vehicle.id)
        return vehicle
    }
}