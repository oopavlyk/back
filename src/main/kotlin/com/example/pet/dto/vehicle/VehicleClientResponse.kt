package com.example.pet.dto.vehicle

import java.util.*

data class VehicleClientResponse(
    val id: Long,
    val color: String,
    val engineSize: Double,
    val yearOfManufacturer: Date,
    val weight: Int,
)
