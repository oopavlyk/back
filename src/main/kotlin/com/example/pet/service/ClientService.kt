package com.example.pet.service

import com.example.pet.dto.client.AddClientRequest
import com.example.pet.dto.client.ClientResponse
import com.example.pet.dto.client.UpdateClientRequest
import org.springframework.data.domain.Page

interface ClientService {
    fun findAll(): List<ClientResponse>

    fun getClient(number: Int, size: Int, sort: String): Page<ClientResponse>

    fun findById(id: Long): ClientResponse

    fun create(request: AddClientRequest)

    fun update(request: UpdateClientRequest)

    fun delete(id: Long)
}
