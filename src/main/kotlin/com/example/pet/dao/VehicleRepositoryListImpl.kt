package com.example.pet.dao

import com.example.pet.dto.vehicle.VehicleClientResponse
import com.example.pet.entities.Vehicle
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Root

class VehicleRepositoryListImpl(
    @PersistenceContext val entityManager: EntityManager
): VehicleRepositoryList {

    override fun get(pageable: Pageable, sort: String): Page<VehicleClientResponse> {
        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        val criteriaQuery: CriteriaQuery<VehicleClientResponse> =
            criteriaBuilder.createQuery(VehicleClientResponse::class.java)
        val root: Root<Vehicle> = criteriaQuery.from(Vehicle::class.java)
        criteriaQuery.multiselect(
            root.get<Long>("id"),
            root.get<String>("color"),
            root.get<String>("engineSize"),
            root.get<Date>("yearOfManufacture"),
            root.get<Int>("weight")
        )
        if (pageable.sort.isSorted)
            if (sort.split('.')[1] == "asc")
                criteriaQuery.orderBy(criteriaBuilder.asc(root.get<Any>(sort.split('.')[0])))
            else
                criteriaQuery.orderBy(criteriaBuilder.desc(root.get<Any>(sort.split('.')[0])))

        val typedQuery = entityManager.createQuery(criteriaQuery)
        val totalCount = typedQuery.resultList.size.toLong()
        typedQuery.maxResults = pageable.pageSize
        typedQuery.firstResult = pageable.pageNumber * pageable.pageSize

        val resultList = typedQuery.resultList
        return PageImpl(resultList, pageable, totalCount)
    }
}