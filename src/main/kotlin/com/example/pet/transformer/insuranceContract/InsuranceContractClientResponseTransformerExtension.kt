package com.example.pet.transformer.insuranceContract

import com.example.pet.dto.insuranceContract.InsuranceContractClientResponse
import com.example.pet.dto.insuranceKit.InsuranceKitResponse
import com.example.pet.dto.vehicle.VehicleClientResponse
import com.example.pet.entities.InsuranceContract


fun InsuranceContract.toInsuranceContractClientResponse(): InsuranceContractClientResponse{
    return InsuranceContractClientResponse(
        vehicleClientResponse = this.vehicle!!.let {
            VehicleClientResponse(
                id = it.id,
                color = this.vehicle!!.color,
                engineSize = this.vehicle!!.engineSize,
                weight = this.vehicle!!.weight,
                yearOfManufacturer = this.vehicle!!.yearOfManufacture
            )
        },
        insuranceKit = (this.insuranceKit).map { insuranceKit ->
            insuranceKit.let {
                InsuranceKitResponse(
                    insuranceKit.id!!,
                    insuranceKit.duration,
                    insuranceKit.compensationPercent,
                    insuranceKit.damageLevel,
                    it.coveredPart)
            }
        }.toMutableList()
    )
}